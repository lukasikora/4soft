import { AppsListContext } from './AppsListProvider'
import { useContext } from 'react'

export const useAppsList = () => {
	const context = useContext(AppsListContext)

	if (context === undefined) throw new Error('useAppsList must be inside a provider with a value')

	return context
}
