export type AppsListEntity = {
	id: number
	name: string
	company: string
}

export type AppsListResponse = AppsListEntity[]

export type AppPath = string;

export type AppDetailsResponse = {
	id: AppPath,
	name: string,
	logo: string,
	company: string,
	number_of_users: number,
	number_of_active_users: number,
	server_address: string,
	admin: {email: string, first_name: string, last_name: string}
}

export type AppsMap = Record<AppPath, AppsListEntity>

export type AppsListStore = undefined | AppsMap

export type IAppsListContext = {
	store: AppsListStore
	fetchAppsList: () => void
	isFetching: boolean
	error: undefined | string
}


export function assertsAppsListStore(appsList: AppsListStore): asserts appsList is AppsMap {
	if (!appsList) {
		throw new Error('Apps store not initialized')
	}
}

