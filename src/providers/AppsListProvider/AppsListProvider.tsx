import React, { createContext, useCallback, useState } from 'react'
import { AppsListResponse, AppsListStore, IAppsListContext, AppsMap } from './types'
import httpClient from '../../utils/httpClient'
import { useSnackbar } from 'notistack'

export const AppsListContext = createContext<IAppsListContext | undefined>(undefined)

const AppsListProvider: React.FC = ({ children }) => {
	const [store, setStore] = useState<AppsListStore>(undefined)
	const [isFetching, setIsFetching] = useState<boolean>(false)
	const [error, setError] = useState<string | undefined>(undefined)

	const { enqueueSnackbar } = useSnackbar()


	const fetchAppsList = useCallback(async () => {

		if (isFetching) {
			return
		}

		try {
			setIsFetching(true)
			setError(undefined)

			const appsList = await httpClient.get<AppsListResponse>(`/list`)
				//simulated longer response time
				.then(resp => new Promise<AppsListResponse>((resolve) => setTimeout(() => resolve(resp.data), 1000)))




			const initStore = appsList.reduce<AppsMap>((appsMap, value) => {
				return {
					...appsMap,
					[value.id]: value
				}
			}, {})

			console.log(initStore)

			setStore(initStore)


		} catch (e: any) {
			setError(e.message)
			enqueueSnackbar('Fetching apps list error', {
				variant: 'error',
			})
		} finally {
			setIsFetching(false)
		}
	}, [enqueueSnackbar, isFetching])

	return (
		<AppsListContext.Provider value={{
			store,
			isFetching,
			error,
			fetchAppsList,
		}}>{children}</AppsListContext.Provider>

	)
}

export default AppsListProvider
