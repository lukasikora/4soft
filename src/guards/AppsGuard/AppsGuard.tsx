import React, { useEffect, useState } from 'react'
import { useAppsList } from '../../providers/AppsListProvider/useAppsList'
import { Box, CircularProgress } from '@mui/material'
import { Navigate } from 'react-router-dom'


const AppsGuard: React.FC = ({ children }) => {

	const { store, isFetching, fetchAppsList, error } = useAppsList()
	const [resolved, setResolved] = useState<boolean | undefined>()


	useEffect(() => {

		if (!store && !error && !isFetching) {
			fetchAppsList()
			return
		}

		if (!store && !error && isFetching) {
			return
		}

		if (store) {
			setResolved(true)
			return
		}

		setResolved(false)

	}, [store, isFetching, error, fetchAppsList])

	if (resolved === undefined) {
		return <Box sx={{ width: '100%', height: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
			<CircularProgress size={100} />
		</Box>
	}

	if (!resolved) {
		return <Navigate to={'/'} replace />
	}

	return (
		<>
			{children}
		</>

	)
}

export default AppsGuard
