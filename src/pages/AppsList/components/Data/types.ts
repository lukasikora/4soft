import { AppsListEntity } from '../../../../providers/AppsListProvider/types'

export type AppsSortedList = {
	[project: string]: AppsListEntity[]
}
