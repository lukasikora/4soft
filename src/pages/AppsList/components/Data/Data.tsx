import React from 'react'
import { Link } from 'react-router-dom'
import { Grid, Paper } from '@mui/material'
import { makeStyles } from 'tss-react/mui'
import { AppsMap } from '../../../../providers/AppsListProvider/types'
import { AppsSortedList } from './types'

const useStyles = makeStyles()(theme => ({
	list: {
		padding: '10px',
	},
	appName: {
		padding: '4px',
	},
	group: {
		marginBottom: '10px',
		listStyleType: 'none'
	},
	link: {
		textDecoration: 'none',
	},
}))


type Props = {
	store: AppsMap
}

const Data: React.FC<Props> = ({store}) => {

	const { classes } = useStyles()

	const sortedAndGrouped = React.useMemo<AppsSortedList>(() => {
		return Object.values(store)
			.sort((a, b) => a.company.localeCompare(b.company))
			.reduce<AppsSortedList>((acc, current) => {
				const group = current.company

				let result: AppsSortedList = { ...acc }

				if (!acc[group]) {
					result[group] = []
				}

				result[group].push(current)

				return result
			}, {})
	}, [store])



	return (
		<>
			<ul className={classes.list}>
				{Object.keys(sortedAndGrouped).map((group) => (
						<li className={classes.group} key={group}>
							<strong>{group}</strong>
							<Grid container spacing={1} direction={{ xs: 'column', sm: 'row' }}>
								{sortedAndGrouped[group].map(({id}) => (
									<Grid item key={id}>
										<Link className={classes.link} to={`/apps/${id}`}>
											<Paper key={id} variant={'outlined'}>
												<p className={classes.appName}>{store[id].name}</p>
											</Paper>
										</Link>
									</Grid>
								))}
							</Grid>
						</li>

					),
				)}
			</ul>
		</>
	)
}

export default Data
