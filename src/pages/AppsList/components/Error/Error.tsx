import React from 'react'
import { Box } from '@mui/material'
import { SentimentVeryDissatisfied } from '@mui/icons-material'


const Error: React.FC = () => {

	return (
		<Box sx={{ width: '100%', height: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
			<Box sx={{m:2, display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column'}}>
				<SentimentVeryDissatisfied fontSize={'large'} />
			  <p>Something went wrong. Please try once again.</p>
			</Box>
		</Box>
	)
}

export default Error
