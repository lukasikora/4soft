import React, { useEffect } from 'react'
import { useAppsList } from 'providers/AppsListProvider/useAppsList'
import { Box, Fab, LinearProgress } from '@mui/material'
import { Replay } from '@mui/icons-material'
import { makeStyles } from 'tss-react/mui'
import Data from './components/Data/Data'
import Error from './components/Error/Error'

const useStyles = makeStyles()(theme => ({
	container: {
		width: '100%',
		height: '100%',
		overflow: 'auto',
		position: 'relative',
	},
	fetchingButton: {
		position: 'fixed',
		right: '20px',
		zIndex: 1,
		top: '70px',
		[theme.breakpoints.up('sm')]: {
			top: '80px',
		},
	},
	fetchingIndicator: {
		position: 'fixed',
		top: '56px',
		[theme.breakpoints.up('sm')]: {
			top: '72px',
		},
		width: '100%',
		zIndex: 1,
	},
}))


const AppsList: React.FC = () => {

	const { error, store, isFetching, fetchAppsList } = useAppsList()
	const { classes } = useStyles()


	useEffect(() => {

		if (store || isFetching || error) {
			return
		}


		fetchAppsList()

	}, [store, fetchAppsList, isFetching, error])


	return (
		<div className={classes.container}>
			<Box className={classes.fetchingButton}>
				<Fab
					aria-label='fetch'
					color='primary'
					onClick={fetchAppsList}
				>
					<Replay />
				</Fab>
			</Box>
			{isFetching && <Box className={classes.fetchingIndicator}>
				<LinearProgress color={'secondary'} />
			</Box>}
			{store && <Data store={store} />}
			{!store && error && <Error />}
		</div>
	)
}

export default AppsList
