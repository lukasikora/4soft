import React, { useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { useApp } from 'pages/AppDetails/useApp'
import { AppPathParams } from './types'
import { makeStyles } from 'tss-react/mui'
import { Box, Fab, LinearProgress } from '@mui/material'
import Data from './components/Data/Data'
import Error from './components/Error/Error'
import { useSnackbar } from 'notistack'
import { Replay } from '@mui/icons-material'

const useStyles = makeStyles()(theme => ({
	container: {
		width: '100%',
		height: '100%',
		overflow: 'auto',
		position: 'relative',
	},
	fetchingButton: {
		position: 'fixed',
		right: '20px',
		zIndex: 1,
		top: '70px',
		[theme.breakpoints.up('sm')]: {
			top: '80px',
		},
	},
	fetchingIndicator: {
		position: 'fixed',
		top: '56px',
		[theme.breakpoints.up('sm')]: {
			top: '72px',
		},
		width: '100%',
		zIndex: 1,
	}
}))

const AppDetails: React.FC = () => {

	const appSelector = useParams<AppPathParams>() as AppPathParams
	let navigate = useNavigate()
	const { enqueueSnackbar } = useSnackbar()

	const { classes } = useStyles()

	const selectedApp = appSelector.app

	const { resolvedApp, fetchAppDetails, details, isFetching, error } = useApp(selectedApp)


	useEffect(() => {
		if (resolvedApp === false) {
			enqueueSnackbar(`App not found: ${selectedApp}`, {
				variant: 'warning',
			})
			navigate('/', { replace: true })
		}

	}, [resolvedApp, navigate, enqueueSnackbar, selectedApp])

	return (
		<div className={classes.container}>
			<Box className={classes.fetchingButton}>
				<Fab
					aria-label='fetch'
					color='primary'
					onClick={fetchAppDetails}
				>
					<Replay />
				</Fab>
			</Box>
			{isFetching && <Box className={classes.fetchingIndicator}>
				<LinearProgress color={'secondary'} />
			</Box>}
			{resolvedApp && details && <Data details={details} fetchAppDetails={fetchAppDetails} /> }
			{resolvedApp && !details && error && <Error fetchAppDetails={fetchAppDetails} /> }
		</div>
	)
}

export default AppDetails
