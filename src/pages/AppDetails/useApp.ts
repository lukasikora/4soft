import { useAppsList } from '../../providers/AppsListProvider/useAppsList'
import { useCallback, useEffect, useState } from 'react'
import httpClient from '../../utils/httpClient'
import { AppDetailsResponse, AppPath } from '../../providers/AppsListProvider/types'
import { useSnackbar } from 'notistack'


const appsCache: Record<AppPath, AppDetailsResponse> = {}

export const useApp = (selector: AppPath) => {

	const { store: appStore } = useAppsList()

	const [isFetching, setIsFetching] = useState<boolean>(false)
	const [error, setError] = useState<string | undefined>(undefined)
	const [details, setDetails] = useState<undefined | AppDetailsResponse>(undefined)
	const [resolvedApp, setResolvedApp] = useState<boolean | undefined>(undefined)

	const { enqueueSnackbar } = useSnackbar()

	useEffect(() => {
		if (!appStore) {
			return
		}
		const appExist = appStore[selector]

		if (!appExist) {
			setResolvedApp(false)
			return
		}

		setResolvedApp(true)

	}, [appStore, selector])

	const fetchAppDetails = useCallback(async () => {

		if (!resolvedApp) {
			return
		}

		if (isFetching) {
			return
		}


		try {
			setIsFetching(true)
			setError(undefined)


			const data = await httpClient.get<AppDetailsResponse>(`/details/${selector}`)
				.then(resp => new Promise<AppDetailsResponse>((resolve) => setTimeout(() => resolve(resp.data), 1000)))


			appsCache[selector] = data
			setDetails(data)

		} catch (e: any) {
			setError(e.message)
			enqueueSnackbar('Fetching app error', {
				variant: 'error',
			})
		} finally {
			setIsFetching(false)
		}
	}, [isFetching, resolvedApp, selector, enqueueSnackbar])


	useEffect(() => {
		if (!resolvedApp) {
			return
		}

		if (!details && error) {
			return
		}

		if (isFetching) {
			return
		}

		if (details) {
			return
		}

		const cache = appsCache[selector]

		if (cache) {
			setDetails(cache)
			return
		}


		fetchAppDetails()

	}, [fetchAppDetails, resolvedApp, details, selector, error, isFetching])


	return {
		details,
		fetchAppDetails,
		error,
		isFetching,
		resolvedApp,
	}
}
