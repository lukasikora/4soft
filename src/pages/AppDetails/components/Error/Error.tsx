import React from 'react'
import { Box, IconButton } from '@mui/material'
import { Replay } from '@mui/icons-material'

type Props = {
	fetchAppDetails: () => void
}

const Error: React.FC<Props> = ({ fetchAppDetails }) => {

	return (
		<Box sx={{ width: '100%', height: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
			<Box sx={{ m: 2, display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
				<IconButton color="primary" aria-label="add to shopping cart" size={"large"} onClick={fetchAppDetails}>
					<Replay />
				</IconButton>
				<p>Something went wrong. Please try once again.</p>
			</Box>
		</Box>
	)
}

export default Error
