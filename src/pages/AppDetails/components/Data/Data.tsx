import React from 'react'
import { Link } from 'react-router-dom'
import { Breadcrumbs, Grid } from '@mui/material'
import { makeStyles } from 'tss-react/mui'
import { AppDetailsResponse, assertsAppsListStore } from '../../../../providers/AppsListProvider/types'
import { useAppsList } from '../../../../providers/AppsListProvider/useAppsList'

const useStyles = makeStyles()(theme => ({
	header: {
		marginBottom: '20px',
		marginTop: '20px',
	},
	gallery: {
		padding: '10px',
	},
}))


type Props = {
	details: AppDetailsResponse
	fetchAppDetails: () => void
}

const Data: React.FC<Props> = ({ details, fetchAppDetails}) => {

	const { classes } = useStyles()

	return (
		<>

				<div>
					<div className={classes.gallery}>
						<>
							<div className={classes.header}>
								<Breadcrumbs aria-label='breadcrumb'>
									<Link to={'/'}>
										<h2>Project</h2>
									</Link>
									<h2 color='text.primary'>{details.id}</h2>
								</Breadcrumbs>
							</div>


							<pre>
								{JSON.stringify(details, null, 2)}
							</pre>


						</>
					</div>
				</div>
		</>
	)
}

export default Data
