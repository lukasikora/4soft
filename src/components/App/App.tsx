import React from 'react'

import { CssBaseline, ThemeProvider} from '@mui/material'
import Navbar from '../Navbar/Navbar'
import theme from 'utils/theme'
import Content from '../Content/Content'
import { BrowserRouter as Router } from 'react-router-dom'
import AppsListProvider from '../../providers/AppsListProvider/AppsListProvider'
import { SnackbarProvider } from 'notistack'


const App: React.FC = () => {
	return (
			<Router>
				<SnackbarProvider maxSnack={3} anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'right',
				}}>
					<AppsListProvider>
							<ThemeProvider theme={theme}>
								<CssBaseline />
								<Navbar />
								<Content />
							</ThemeProvider>
					</AppsListProvider>
				</SnackbarProvider>
			</Router>
	)
}

export default App
