import React from 'react'
import {
	Tabs as MuiTabs,
	Tab,
} from '@mui/material'

import { Link } from 'react-router-dom'

import { Favorite, List } from '@mui/icons-material'
import { makeStyles } from 'tss-react/mui'

const useStyles = makeStyles()(theme => ({
	icons: {
		fontSize: '1.4rem',
	},
}))

type Props = {
	currentTab: false | string
}

const Tabs: React.FC<Props> = ({ currentTab }) => {
	const { classes } = useStyles()

	return (
		<MuiTabs
			value={currentTab}
		>
			{[
				{
					path: '/',
					label: 'List',
					IconComponent: List
				}
				].map(({ label, path, IconComponent }) => (
					<Tab
						icon={<IconComponent className={classes.icons} />}
						label={label}
						value={path}
						to={path}
						component={Link}
						key={path}
					/>
				),
			)}
		</MuiTabs>
	)
}

export default Tabs
