import React from 'react'
import {
	AppBar,
	Toolbar,
	useMediaQuery,
	useTheme,
} from '@mui/material'

import { useMatch } from 'react-router-dom'
import Drawer from './Drawer/Drawer'
import Tabs from './Tabs/Tabs'
import { SiDatadog } from 'react-icons/si'
import { makeStyles } from 'tss-react/mui'

const useStyles = makeStyles()(theme => ({
	logo: {
		fontSize: '1.9rem',
		[theme.breakpoints.down('md')]: {
			fontSize: '1.1rem',
		},
	},
	tabsContainer: {
		marginLeft: 'auto',
	},
	iconLogoContainer: {
		display: 'flex',
	},
	iconLogo: {
		color: 'white',
		fontSize: '3rem',
	},
}))

const Navbar = () => {
	const { classes } = useStyles()

	const theme = useTheme()

	const isMatch = useMediaQuery(theme.breakpoints.down('sm'))

	const appMatch = useMatch('/')


	const currentTab = (appMatch)?.pattern?.path ?? false

	return (
		<>
			<AppBar elevation={0} color='primary'>
				<Toolbar>
					<div className={classes.iconLogoContainer}>
						<SiDatadog className={classes.iconLogo} />
					</div>
					{isMatch ? (
						<>
							<Drawer currentTab={currentTab} />
						</>
					) : (
						<div className={classes.tabsContainer}>
							<Tabs currentTab={currentTab} />
						</div>
					)}
				</Toolbar>
			</AppBar>
		</>
	)
}

export default Navbar
