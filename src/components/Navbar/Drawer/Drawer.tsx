import React, { useState } from 'react'
import {
	List,
	ListItem,
	ListItemIcon,
	IconButton,
	ListItemText,
	Drawer as MuiDrawer, ListItemButton,
} from '@mui/material'

import { Menu, List as ListIcon, Favorite } from '@mui/icons-material'
import { makeStyles } from 'tss-react/mui'
import { Link } from 'react-router-dom'

const useStyles = makeStyles()(theme => ({
	drawerContainer: {},
	iconButtonContainer: {
		marginLeft: 'auto',
	},
	menuIconToggle: {
		fontSize: '3rem',
		color: 'white',
	},
}))

type Props = {
	currentTab: false | string
}

const Drawer: React.FC<Props> = ({ currentTab }) => {

	const [openDrawer, setOpenDrawer] = useState(false)

	const { classes } = useStyles()

	return (
		<>
			<MuiDrawer
				anchor='right'
				classes={{ paper: classes.drawerContainer }}
				onClose={() => setOpenDrawer(false)}
				open={openDrawer}>
				<List>
					{[
						{
							path: '/',
							label: 'List',
							IconComponent: ListIcon
						},
						].map(({ label, path,  IconComponent}) => (
							<ListItem divider onClick={() => setOpenDrawer(false)} key={path}>
								<ListItemButton component={Link} to={path} selected={currentTab === path}>
									<ListItemIcon>
										<IconComponent />
									</ListItemIcon>
									<ListItemText>{label}</ListItemText>
								</ListItemButton>
							</ListItem>
						),
					)}


				</List>
			</MuiDrawer>
			<div className={classes.iconButtonContainer}>
				<IconButton
					onClick={() => setOpenDrawer(!openDrawer)}
					disableRipple>
					<Menu className={classes.menuIconToggle} />
				</IconButton>
			</div>

		</>
	)
}

export default Drawer
