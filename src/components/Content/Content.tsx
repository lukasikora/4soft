import React, {lazy} from 'react'
import { Routes, Route, Navigate, Outlet } from 'react-router-dom'
import AppsList from 'pages/AppsList/AppsList'

import { makeStyles } from 'tss-react/mui'
import AppsGuard from '../../guards/AppsGuard/AppsGuard'


const useStyles = makeStyles()(theme => ({
	container: {
		height: '100vh',
		width: '100%',
		paddingTop: '56px',
		[theme.breakpoints.up('sm')]: {
			paddingTop: '72px',
		},
	},
	content: {
		overflow: 'hidden',
		width: '100%',
		height: '100%',
	},
}))

const LazyAppDetails = lazy(() => import('pages/AppDetails/AppDetails'))


function Content() {

	const { classes } = useStyles()

	return (
		<div className={classes.container}>
			<div className={classes.content}>
				<Routes>
					<Route path='/' element={<AppsList />} />
					<Route path='apps' element={
						<AppsGuard>
							<Outlet />
						</AppsGuard>
					}>
						<Route path=':app' element={
							<React.Suspense fallback={null}>
								<LazyAppDetails />
							</React.Suspense>
						} />
					</Route>
					<Route
						path='*'
						element={<Navigate to='/' replace />}
					/>
				</Routes>
			</div>
		</div>
	)
}

export default Content
