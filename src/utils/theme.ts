import { createTheme } from '@mui/material'

const theme = createTheme({
	components: {
		MuiTabs: {
			styleOverrides: {
				indicator: {
					'&': {
						backgroundColor: 'white'
					}
				}
			}
		},
		MuiTab: {
			styleOverrides: {
				root: {
					'&': {
						opacity: 0.4,
						color: 'white'
					},
					'&.Mui-selected': {
						opacity: 1,
						color: 'white'
					},
				},
			},
		},
	},
})

export default theme
