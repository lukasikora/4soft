import axios from 'axios'


export default axios.create({
	baseURL: 'https://api.recruitment.4soft.tech/'
})
